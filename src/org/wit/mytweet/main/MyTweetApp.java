package org.wit.mytweet.main;

import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.PortfolioSerializer;
import static org.wit.mytweet.helpers.LogHelpers.info;

import android.app.Application;

public class MyTweetApp extends Application //Launch MyTweetApp
{

  public Portfolio portfolio;
  private static final String FILENAME = "portfolio.json";

  @Override
  public void onCreate()
  {
    super.onCreate();
    PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
    portfolio = new Portfolio(serializer);

    info(this, "MyTweet app launched");
  }

}

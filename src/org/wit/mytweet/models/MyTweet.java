package org.wit.mytweet.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.wit.mytweet.R;

import android.content.Context;

public class MyTweet
{

  public UUID id;
  public String tweet_text;
  public Date date;
  public String select_contact;

  private static final String JSON_ID = "id";
  private static final String JSON_TWEET_TEXT = "tweet_text";
  private static final String JSON_DATE = "date";
  private static final String JSON_SELECT_CONTACT = "select_contact";

  public MyTweet()
  {
    id = UUID.randomUUID();
    this.date = new Date();
    this.select_contact = "";
  }

  public MyTweet(String tweet_text, Date date, String select_contact)
  {
    id = UUID.randomUUID();
    this.tweet_text = tweet_text;
    this.date = date;
    this.select_contact = select_contact;
  }

  public MyTweet(JSONObject json) throws JSONException
  {
    id = UUID.fromString(json.getString(JSON_ID));
    tweet_text = json.getString(JSON_TWEET_TEXT);
    date = new Date(json.getLong(JSON_DATE));
    select_contact = json.getString(JSON_SELECT_CONTACT);

  }

  public JSONObject toJSON() throws JSONException
  {
    JSONObject json = new JSONObject();
    json.put(JSON_ID, id.toString());
    json.put(JSON_TWEET_TEXT, tweet_text);
    json.put(JSON_DATE, date.getTime());
    json.put(JSON_SELECT_CONTACT, select_contact);
    return json;
  }

  public String getDateString()
  {
    return "Tweet date: " + DateFormat.getDateTimeInstance().format(date);
  }

  public String getTweet(Context context)
  {
    String tweetString = null;
    tweetString = context.getString(R.string.mind);
    return tweetString;
  }

}

package org.wit.mytweet.models;

import java.util.ArrayList;
import java.util.UUID;

import org.wit.mytweet.models.PortfolioSerializer;

import android.util.Log;
import static org.wit.mytweet.helpers.LogHelpers.info;

public class Portfolio
{

  public ArrayList<MyTweet> tweets;
  private PortfolioSerializer serializer;

  public Portfolio(PortfolioSerializer serializer)
  {
    this.serializer = serializer; //initialises tweet serialisation
    try
    {
      tweets = serializer.loadTweets();
    }
    catch (Exception e)
    {
      info(this, "Error loading residences: " + e.getMessage());
      tweets = new ArrayList<MyTweet>();
    }
  }

  public boolean saveTweets()
  {
    try
    {
      serializer.saveTweets(tweets);
      info(this, "Tweets saved to file");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error saving tweets: " + e.getMessage());
      return false;
    }
  }

  public MyTweet getTweet(UUID id)
  {
    Log.i(this.getClass().getSimpleName(), "UUID parameter id: " + id);

    for (MyTweet tweet : tweets)
    {
      if (id.equals(tweet.id))
      {
        return tweet;
      }
    }
    return null;
  }

  public void newTweet(MyTweet tweet)
  {
    tweets.add(tweet);
  }

  public void deleteTweet(MyTweet tweet)
  {

    tweets.remove(tweet);

  }

}
